<?php
    session_start();
    require_once('config.php');
    $con=mysqli_connect($db_host,$db_username,$db_password,$db_name);
    if (mysqli_connect_errno())
    {
        echo "MySQL Connection Failed, please contact the game master\nTechnical details: " . mysqli_connect_error();
    }
    