<!DOCTYPE html>
<html>
    <head>
      <title>HG Assassin</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta charset="UTF-8">
     </head>
     <body>
		<h3>Lazy?  <a href="https://docs.google.com/document/d/1V5ASV3HyQI21_lHbt9m2rqsoci7sIu9BQTLMArCQ7rU/edit?usp=sharing"> Read this</a></h3>
        Please send your questions to <a href="mailto:inquiries@hgassassin.com">inquiries@hgassassin.com</a>
        <h3>Assassin will run from May 12th at Noon to June 15th at midnight
        <h1>Properties: Read this to your parents!</h1>
            <p>
                Before signing up, please clarify with your parents that there will be people that will come onto your property.  If anybody (excluding the target and their partner) asks a player to leave, that player must leave for 3 hours.  No assassin is permitted to enter any fully enclosed structures on the target's property unless they are welcomed in by the target or any person who lives in the house.  Fully enclosed means any structure with a roof that can only be entered through a doorway or a garage door (ex. house, shed, garage, basement).  Fenced and mosquito netted areas do not count, so you may enter them if you can do so without destroying property.  If you can hit your target without entering the structure, you may do so (ex. the target opens the door).
            </p>
        <h2><a name="Basics">Basics</a></h2>
            <ul>
                <li>At the start of the game, on May 12th at Noon, your team will be assigned two targets that are partners</li>
                <li>You must wipe out a partnership to receive more targets.  A partnership won't necessarily have two living targets except for your first set of targets</li>
                <li>Once you have assassinated both targets, your partnership will receive the targets of the targets you have just assassinated</li>
                <li>At all times, you will have target(s) to go after, and another partnership (you aren't told who) will have you as a target</li>
                <li>To stay in the game, your team must have at least one confirmed kill before Monday, May 19th at 12:01 AM.  If you submitted a kill before 12:01AM and it is confirmed after 12:01AM, you are still in the game</li>
                <li>You must have your ID card in digital or paper form at all times, failure to produce the card in the event of your assassination will cause delays in the game flow.  Failing to produce the card may result in ineligibility for special revival bonuses!</li>
                <li>It is up to you to read and understand the rules of the game, this game is only fun when everybody is on the same page</li>
                <li>You can deceive with decoys and false rumours, but do not give people the false notion that they have been killed by somebody who is not their real assassin.  Do not tell people you got them if you aren't assigned them</li>
            </ul>
        <h2><a name="Prizes">Prizes</a></h2>
            <p>
                The living team with the most aggregate kills will be awarded the prize.  The prize amount will vary based on how many people sign up, it will be announced at a later time.  In the event of a total annihilation with only one team remaining, there will be two prizes.  The last standing team will receive a prize as well as the team with the most kills.  If the last standing team happens to also be the team with the most kills, they will be awarded the entire prize.  Prizes will be distributed to the teams, not to individuals.  Please establish a way of sharing the prize money with your partner!  If you wish to break up the prize money based on kills per person, we will provide you with the official kill counts.  May the odds be ever in your favor.  (Please don't sue me Scholastic Press!)
            </p>
        <h2>Game Dynamics</h2>
            <p>
                In order to move the game along in a timely manner, there will be periodic changes to the rules.  Every Monday of the killing period, there will be an updated set of rules.  Rules that will be enacted include but are not limited to kill minimums, revival conditions, and special safezones.  Added rules remain in effect until otherwise specified in the next Monday set of rule amendments.  The new set of rules will be announced via email as early as possible.  On the Monday following the announcement, they become immutable game rules.  However, before that Monday, supplemental announcements can be made to update the rules before they go into effect.  This page will be updated to link to the supplemental rules as they are made permanent.  Minor changes in order to correct contradictory statements or loopholes can be added at any time, so please read any emails from the game master as they may include important rule changes that take effect immediately.
            </p>
        <h2><a name="Scoring">Scoring</a></h2>
            <p>
                Although we ask that you report kills by who fired the water gun or threw the water balloon, the ultimate winners will be decided by team scoring.  Unless otherwise noted, scores are by aggregate kills of the partnership.  If your partner is assassinated, they aren't necessarily gone for the remainder of the game, the top scoring teams will receive bonuses based on the state of the game.  A fallen partner's kills will still count towards your score.
            </p>
        <h2><a name="SafeZones">Safe Zones & Immunity Cases</a></h2>
            <ul>
                <li>School Grounds.  All Greeley property is safe at all times (includes Walkabout campus).  Don't even bring your assassination weapons into school, leave them at home</li>
                <li>Buses to and from Greeley or any school events</li>
                <li>Places where school activities are being conducted such as Casa De Lengua or anywhere that school chaperones will be present</li>
                <li>Places of worship</li>
                <li>Vehicles in certain cases(see <a href="#Vehicles">Vehicles</a>)</li>
                <li>Workplaces.  We don't want anybody being fired over this game, please do not attack people who are at their workplace</li>
                <li>Volunteer firefighters or medical while they are at or on the way to an emergency</li>
                <li>To prevent ruining work clothes, you may not shoot your target when they are about to enter their workplace.  You may only attack after they have left the workplace (ex. in the parking lot)</li>
                <li><u>All</u> parking lots are fair game except the ones on Greeley property</li>
                <li>Workplace rules do not include babysitters, landscaping, or the self-employed</li>
                <li>Volunteer work is safe if it is conducted indoors, such as a daycare center</li>
                <li>Senior experience interning and any other internships count as would a regular job, you are safe when you are at your internship</li>
            </ul>
        <h2><a name="Witnesses">Witnesses</a></h2>
            <ul>
                <li>For a kill to count, there must be no witnesses!</li>
                <li>Any Seniors from Horace Greeley can count as witnesses.  This includes lifeschool and walkabout</li>
                <li>If the senior is directly involved in the kill (eg. camera(wo)man) they do not count</li>
                <li>The Senior does not have to be dead, alive, or playing at all.  They may not be the partner of the assassin or the victim</li>
                <li>If the Senior <u>sees</u> you assassinate your target, your kill will not count!</li>
                <li>Clarification: you must see the perpetrator AND the victim at the moment that the kill takes place.  If the assassin shoots from behind cover, and you can't see them, that kill counts!</li>
                <li>The Senior has to see the assassination take place, simply being in the vicinity does not count</li>
                <li>Ideally, you should accept defeat if the Senior sees you do it: be honest please</li>
                <li>The victim should identify the Senior who witnessed the assassination in the event of a dispute</li>
            </ul>
        <h2><a name="Misc">Miscellaneous Rules</a></h2>
            <ul>
                <li>If you plan to travel outside a reasonable driving distance from Chappaqua, contact the game master or you will risk disqualification</li>
                <li>You will be disqualified if you intentionally cause any physical harm to another player</li>
            </ul>
        <h2><a name="Signup">Sign up</a></h2>
            <p>
                Prepare exactly $20 in cash (go easy on the $1 bills) and secure it in a way that it can be transported.  You may also request to pay via PayPal for an additional ~$0.91 to cover the fee.  With the money, include your and your partner's full names (first and last), email, and phone number.  Your email and phone number will only be used for the duration of the game to alert you of game events and important information.  To ensure that you can receive emails, please add <i>gamemaster@hgassassin.com</i> to your address-book or white-list.  Your phone number is optional, it will be used for SMS updates that may cost you money depending on your SMS provider.  Please don't supply a phone number if you will be charged or don't want to receive SMS.
            </p>
        <h2><a name="Weapons">Weapons</a></h2>
            <p>
                Water <b>(PURE WATER! NO MIXTURES)</b> is your assassination medium, use it wisely.  Water balloons may be used but they must explode <u><b>on</b></u> the target, not near the target.  You can use whatever weapon you want to hurl water at people as long as it does not cause pain or cause excessive drenching.  Use common sense here, a 5 gallon bucket is excessive, while a 500ml water bottle is reasonable.  Keep the capacity of your water weapon under 2 Liters (capacity of a full size soda bottle).  The goal is to use enough water to have fun but not give your target hypothermia and a water damaged phone.  You can use any hand operated water weapons such as a super soaker, but you <b>may not</b> use any electric/combustion powered water streams such as a hose or a power washer.  You can however, use hand-held battery powered water guns.  It's recommended that you buy a small, one-handed water weapons.  You can also improvise if necessary with a cup or a water bottle with a hole poked in the top.  <u>Do not push people into pools, spit water on them, ask them to drink water, or set booby traps, these methods do not count as kills!</u>
            </p>
        <h2><a name="Reporting Kills">Reporting Kills</a></h2>
            <p>
                Once you've hit your target in water in a way that follows the rules, it's now time to report your glorious victory.  Email <a href="mailto:gamemaster@hgassassin.com">gamemaster@hgassassin.com</a> and include your name and the name and ID card code of the person who you just killed.  Failure to provide an ID card code will delay your kill confirmation for up to 24 hours or more.  Any QR reader for smartphones should be able to scan the ID card code of the victim (it would be wise to make sure your app can scan your own ID card).  If you are the victim and the kill was clearly valid, please provide your QR code on your phone or on paper to your assassin.  In the event of a dispute, please try to resolve it civilly.  If no resolution is met, email the Game Master with a description of the events that took place and a list of the parties involved.  We will try to resolve the dispute as quickly as possible, but it will take time.
            </p>
        <h2>Defending Yourself</h2>
            <p>
                You will have people hunting you down, trying to assassinate you in order to advance themselves.  You are not helpless if you spot somebody on the attack, you may perform what is called a defensive kill.  If you notice your assassin hunting or actively attacking you, you may use approved water weapons to hit them before they can get you.  If you do hit them, they must <u><b>stop hunting you</b></u> for 3 hours.  Make sure to record the time that you hit them, and inform them of the rules.  In the event of a tie, the target gets the defensive kill.  Please be civil and honest, after the 3 hours you can return and try again.  Remember that the partner of the attacker can still come after you, the 3 hours applies only to individuals.  The assassin should leave the general area in which they attacked you, and not return for the three hours.
            </p>
        <h2><a name="Vehicles">Vehicles</a></h2>
        <p>
            Any vehicle (motorcycle, bus, car, etc.) becomes a safe zone whenever the engine or motor is on.  Safe zone means you can't get hit while in it, or shoot from it.  This includes electric cars and hybrid vehicles when they are on and ready to move but their engines have been turned off by the on-board computer.  As soon as the vehicle is turned off by the driver, the vehicle becomes like any other place.  Use common sense while making assassinations in a car, don't needlessly ruin the upholstery of the car.  You may enter or open the door of any unlocked car that is in park and off to assassinate your target.  Means of transportation without a motor or engine do not count as safe zones, you may shoot skateboarders, horseback riders, rollerbladers, etc.  Assassinations of people who are on bicycles or other means of transportation that require balance may only be assassinated with streams of water in order to prevent injury from being knocked off balance by a water balloon.
        </p>
        <h2>Bonuses</h2>
            <p>
                Here comes the taboo subject...  If you record video of your successful kill and the target is clearly breaking the rules (refusing to admit they were hit)  you will receive an extra kill point.  If this happens, do try to argue your case but if the person makes threats or leaves, please send us the video.  We hope that all the players will be honest but in the event that you can catch somebody's malicious act, you will receive the bonus.  To protect the dignity of the dishonest player, we ask that you do not share the video with anyone but the Game Master.  We will not disclose anything about the event to anybody, the points will be added in secret.  This rule is added to protect players who wish to follow the rules and punish those who selfishly ignore the rules.  I don't want to receive any of these videos.
            </p>
        <h2><a name="Disclaimer">Disclaimer</a></h2>
            <p>
                We are not responsible for any property damage, injury, or loss of life.  You must inform anybody who may be affected by these rules (ex. your parents) before signing up.  Play this game at your own risk.  All decisions made by the game master is final.  No refunds will be administered for players that are disqualified or ruled "killed" at any time.  Although you will get wet, players are expected to be prudent with water.  You may be disqualified at any time if deemed necessary.
            </p>
     </body>