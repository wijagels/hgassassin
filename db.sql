/*Schema v1.0*/
CREATE DATABASE IF NOT EXISTS `login`;
USE `login`;

CREATE TABLE IF NOT EXISTS `targets` (
  `UID` int(10) unsigned NOT NULL,
  `Target UID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`UID`),
  KEY `tgtuid` (`Target UID`),
  CONSTRAINT `tgtuid` FOREIGN KEY (`Target UID`) REFERENCES `users` (`UID`),
  CONSTRAINT `userid` FOREIGN KEY (`UID`) REFERENCES `users` (`UID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `users` (
  `UID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Partner UID` int(10) unsigned DEFAULT NULL,
  `Name` tinytext NOT NULL,
  `Email` tinytext NOT NULL COMMENT 'RegEx validation should happen at php layer',
  `Password` text NOT NULL COMMENT 'Store HASHED AND SALTED password',
  `Picture` mediumblob NOT NULL COMMENT 'Contains raw picture automatically resized to whatever it should be upon upload',
  `Paid for` bit(1) NOT NULL COMMENT 'Did they pay yet?  Check this often!',
  `Heartbeat` bit(1) NOT NULL COMMENT 'Do they have a pulse?  Are they alive!?',
  PRIMARY KEY (`UID`) USING HASH,
  UNIQUE KEY `UID` (`UID`),
  UNIQUE KEY `Partner UID` (`Partner UID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains all the users and their Identification Details.';
