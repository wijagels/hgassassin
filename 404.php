<?php
	header('Refresh: 3; URL=./');
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Uhhhh, whoops.</title>
		<meta charset="UTF-8">
	</head>
	<body>
		<div style="text-align:center">
			<h1>404 Not Found!  Sending you to the home page in a moment...</h1>
			<h3>Blame this guy.</h3>
			<img src="/img/404.jpg" alt="I can't see any pages!" height="256"><br>
			Impatient? <a href="./">Click me.</a>
		</div>
	</body>
</html>