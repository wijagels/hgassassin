<?php   session_start();    ?>
<!DOCTYPE html>
<html>
    <head>
      <title>HG Assassin</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta charset="UTF-8">
      <!-- Bootstrap -->
      <link href="bootstrap/bootstrap.min.css" rel="stylesheet" media="screen">
      <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
      <link href="css/bootflat.css" rel="stylesheet" media="screen">
      <link href="css/bootflat-extensions.css" rel="stylesheet" media="screen">
      <link href="css/bootflat-square.css" rel="stylesheet" media="screen">
      <link href="css/bootflat-extensions.css" rel="stylesheet" media="screen">
      <link href="css/bootflat-square.css" rel="stylesheet" media="screen">

      <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
      <![endif]-->
    </head>
    <body>
      <div class="alert">
         <div style="text-align:center">
	 <h1>The Horace Greeley Assassin game WebUI<br>Under Construction...</h1>
         <br><br><h3>Below are logos of the software planned to be used to make this work</h3>
         </div>
       </div>

      <!-- jQuery right now trying the google hosted version, not sure if correct version -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="js/bootstrap.min.js"></script>
<footer>
<div style="text-align:center">
<a href="http://www.python.org/">
	<img src='img/soft/python.svg' alt='python' height='64'/>
</a>
<a href="http://www.w3.org/">
	<img src='img/soft/html5.svg' alt='html5' height='64'/>
	<img src='img/soft/css3.svg' alt='css3' height='64'/>
</a>
<a href="https://php.net/">
	<img src='img/soft/php.svg' alt='php' height='64'/>
</a>
<a href="https://www.mysql.com/">
	<img src='img/soft/mysql.svg' alt='mysql' height='64'/>
</a>
<a href="https://httpd.apache.org/">
	<img src='img/soft/apache.svg' alt='apache' height='64'/>
</a>
<a href="https://www.freebsd.org/">
	<img src='img/soft/freebsd.png' alt='freebsd' height='64'/>
</a>
<a href="http://validator.w3.org/check?uri=referer">
	<img src="img/soft/w3.svg" alt='w3 validate' height='64'/>
</a>
<a href="https://www.gnu.org/licenses/gpl.html">
	<img src="img/soft/gplv3.svg" alt='gplv3' height='64'/>
</a>
<br><h6>All the buttons!</h6><br>
<?php
$load = sys_getloadavg();
$mem = memory_get_usage()/1024;
echo 'Memory Usage: ';
echo $mem;
echo ' kb';
?>
</div>
<div style="text-align:right">
<img src="https://upload.wikimedia.org/wikipedia/commons/7/76/FSM_Logo.svg" height="32" alt="long live FSM">
</div>
</footer>
</body>
</html>
